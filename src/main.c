/**
 * @file main.c
 * This is the main file for the 2015 Advent of code project
 * @author Eoghan Conlon
 * @date Created: 25/11/2022 Last Modified: 26/11/2022
 */

#include "utils.h"

/**
 *
 * @return EXIT SUCCESS if executed successfully, EXIT FAILURE if an error is encountered
 */
int main(){
    FILE *input;
    char line[80];
    char c;

    input = fileOpener(input, 0);
    if(input == NULL){
        perror("file.txt");
        EXIT_FAILURE;
    }
    while((c = getc(input)) != EOF){
        printf("%c", c);
    }
    EXIT_SUCCESS;
}