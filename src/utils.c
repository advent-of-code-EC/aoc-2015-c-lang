/**
 * @file utils.c
 * This is the utilities file for the AOC C Lamg project
 * @author Eoghan Conlon
 * @date Created: 26/11/2022 Last modified: 26/11/2022
 */

#include "utils.h"

FILE* fileOpener(FILE* file, int day){
    switch(day){
        default:{
            file = fopen("../input/test.txt", "r");
        }
    }
    return file;
}