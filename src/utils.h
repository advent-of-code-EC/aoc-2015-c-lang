/**
 * @file utils.h
 * Header file for AoC 2015 CLang project
 *
 * @author Eoghan Conlon
 *
 * @date created: 26/11/2022 last modified: 26/11/2022
 */

#ifndef INC_2015_C_UTILS_H
#define INC_2015_C_UTILS_H
#include <stdio.h>
#include <stdlib.h>

FILE* fileOpener(FILE* file, int day);
#endif //INC_2015_C_UTILS_H
