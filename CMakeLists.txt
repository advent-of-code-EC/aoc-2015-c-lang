cmake_minimum_required(VERSION 3.18)
project(2015_c)

set(CMAKE_C_STANDARD 11)

add_subdirectory(src)